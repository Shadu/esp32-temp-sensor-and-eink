#include <AsyncWebSocket.h>
#include <dataUpdate.h>

#define MAX_PRINTF_LEN 64

AsyncWebSocket webS("/ws");
uint32_t clientID = 0;

void socketPrintf(const char *format, ...)
{
    if (clientID != 0)
    {
        va_list arg;
        va_start(arg, format);
        webS.client(clientID)->printf(format, arg);
        va_end(arg);
        return;
    }
    else
    {
        return;
    }
}

void clientPrintf(uint32_t id, const char *format, ...)
{
    if (id != 0)
    {
        va_list arg;
        va_start(arg, format);
        webS.client(id)->printf(format, arg);
        va_end(arg);
        return;
    }
    else
    {
        return;
    }
}