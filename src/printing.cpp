#include <Adafruit_GFX.h> // Core graphics library
#include <Fonts/FreeMonoBold9pt7b.h>
#include <GxEPD2_3C.h>
#include <GxEPD2_BW.h>

#include <Arduino.h>
#include <dataUpdate.h>
#include <drawing.h>
#include <printData.h>
#include <webs.h>

#include "timers.h"
#include "wifi.h"

/**************Display stuff**************/
#define ENABLE_GxEPD2_GFX 0
#define MAX_DISPLAY_BUFFER_SIZE 800
#define MAX_HEIGHT(EPD)                                                        \
  (EPD::HEIGHT <= MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8)                   \
       ? EPD::HEIGHT                                                           \
       : MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8))
GxEPD2_3C<GxEPD2_290c, GxEPD2_290c::HEIGHT>
    display(GxEPD2_290c(/*CS=5*/ SS, /*DC=*/17, /*RST=*/16, /*BUSY=*/4));
/*****************************************/

int error = 0; // used to track what errors are going on and if an error has
               // been printed to the display.
boolean secondProbe = false;

// Function to get the height of the text to be printed to calculate at what
// pixel the cursor needs to be set.
uint16_t getBounds(char *i) {
  int16_t tbx, tby;
  uint16_t tbw, tbh;
  char pok[5];
  strcpy(pok, i);
  display.getTextBounds(pok, 0, 0, &tbx, &tby, &tbw, &tbh);
  return tbh;
}

// Function to do partial prints based on the values submitted. Only one value
// being printed. d - minMaxData Struct. k - If data has been updated/changed.
void partialPrint(minMaxData &d, int k) {

  if (k != 1) {
    return;
  }
  display.firstPage();
  do {
    display.setRotation(1);
    display.setPartialWindow(d.x, d.y, d.w, d.h);
    display.setFont(&FreeMonoBold9pt7b);
    display.setTextColor(GxEPD_BLACK);
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(d.x, (d.y + getBounds(d.ctemp) + 2));
    display.print(d.ctemp);
  } while (display.nextPage());
}

// Function to do partial prints based on the values submitted. Two value's
// being printed. d - minMaxData Struct. j - Time value. H - 0 is for vertical
// print, 1 is for horizontal print. k - If data has been updated/changed.
void partialPrint(minMaxData &d, char *j, int H, int k) {
  if (k != 1) {
    return;
  }
  display.firstPage();
  do {
    display.setRotation(1);
    display.setPartialWindow(d.x, d.y, d.w, d.h);
    display.setFont(&FreeMonoBold9pt7b);
    display.setTextColor(GxEPD_BLACK);
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(d.x, (d.y + getBounds(d.ctemp) + 2));
    display.print(d.ctemp);
    if (H == 0) {
      display.setCursor((display.getCursorX() + 5), display.getCursorY());
      display.print(j);
    } else if (H == 1) {
      display.setCursor(d.x, (display.getCursorY() + getBounds(d.fTemp) + 2));
      display.print(d.fTemp);
    }
  } while (display.nextPage());
}

// Function to make sure the temperature is always 2 digits.
// i - char to store the temperature in.
// j - temperature to store into the char.
// h - the symbol for temperature.
void printTemp(char *i, float j, const char *h) {
  char *ptr = i;
  dtostrf(j, 4, 1, ptr);
  size_t len = strlen(ptr);
  ptr += len;
  strcpy(ptr, h);
}

// Printing function to print the weatherdata and to print the inside sensor
// data.
void print_outside_data(int errorCode) {
  if (errorCode == 0) {
    // Eindhoven temps.
    printTemp(eindHovenData.ctemp, weatherStationOne.temperature,
              temperatureSymbol);
    printTemp(eindHovenData.fTemp, weatherStationOne.feelTemp,
              temperatureSymbol);
    partialPrint(eindHovenData, eindHovenData.fTemp, 1,
                 updateRefresh.ehvUpdate);

    // Ell temps.
    printTemp(eLLData.ctemp, weatherStationTwo.temperature, temperatureSymbol);
    printTemp(eLLData.fTemp, weatherStationTwo.feelTemp, temperatureSymbol);
    partialPrint(eLLData, eLLData.fTemp, 1, updateRefresh.eLLuPdate);

    // Min/max outside temps (Ell based).
    printTemp(maxTempOutData.ctemp, outSide.maxTemp, temperatureSymbol);
    printTemp(minTempOutData.ctemp, outSide.minTemp, temperatureSymbol);
    partialPrint(maxTempOutData, outSide.maxTime, 0,
                 updateRefresh.outMaxUpdate);
    partialPrint(minTempOutData, outSide.minTime, 0,
                 updateRefresh.outMinUpdate);
    updateRefresh.ehvUpdate = updateRefresh.eLLuPdate =
        updateRefresh.outMaxUpdate = updateRefresh.outMinUpdate = 0;
  }
  if (errorCode == 1) {
    updateRefresh.ehvUpdate = updateRefresh.eLLuPdate =
        updateRefresh.inHumidUpdate = updateRefresh.inMaxUpdate =
            updateRefresh.inMinUpdate = updateRefresh.inTempUpdate =
                updateRefresh.outMaxUpdate = updateRefresh.outMinUpdate = 1;
    partialPrint(errorOut, errorOut.time, 1, 1);
    error = 5;
  }
  display.powerOff();
  Serial.println("printing outside done");
  socketPrintf("printing outside done");
}

// Printing function to print the weatherdata and to print the inside sensor
// data.
void print_inside_data(int errorCode) {
  if (errorCode == 0) {
    // Current inside temp.
    printTemp(insideTemp.ctemp, inSide.Temp, temperatureSymbol);
    partialPrint(insideTemp, updateRefresh.inTempUpdate);

    // Current inside humidity.
    sprintf(insideHumid.ctemp, "%d%s", inSide.humidity, "%");
    partialPrint(insideHumid, updateRefresh.inHumidUpdate);

    // Min/max inside temps.
    printTemp(maxTempInData.ctemp, inSide.maxTemp, temperatureSymbol);
    printTemp(minTempInData.ctemp, inSide.minTemp, temperatureSymbol);
    partialPrint(maxTempInData, inSide.maxTime, 0, updateRefresh.inMaxUpdate);

    partialPrint(minTempInData, inSide.minTime, 0, updateRefresh.inMinUpdate);
    updateRefresh.inTempUpdate = updateRefresh.inHumidUpdate =
        updateRefresh.inMaxUpdate = updateRefresh.inMinUpdate = 0;
  }
  if (errorCode == 1) {
    updateRefresh.ehvUpdate = updateRefresh.eLLuPdate =
        updateRefresh.inHumidUpdate = updateRefresh.inMaxUpdate =
            updateRefresh.inMinUpdate = updateRefresh.inTempUpdate =
                updateRefresh.outMaxUpdate = updateRefresh.outMinUpdate = 1;
    partialPrint(errorIn, errorIn.time, 1, 1);
    error = 5;
  }
  display.powerOff();
  Serial.println("printing inside done");
  socketPrintf("printing inside done");
}

// Printing the provided data to the display.
// d - printData struct.
void rendering(minMaxData &d) {
  int16_t tbx, tby;
  uint16_t tbw, tbh;
  delay(10);
  display.getTextBounds(d.ctemp, 0, 0, &tbx, &tby, &tbw, &tbh);
  display.setCursor(d.x, (d.y + tbh + 2));
  display.print(d.ctemp);
}

// Printing the provided data to the display.
// d - printData struct.
// j - time in char.
// i - 1 for vertical print, 2 for horizontal print.
void rendering(minMaxData &d, char *j, int i) {
  int16_t tbx, tby;
  uint16_t tbw, tbh;
  delay(10);
  display.getTextBounds(d.ctemp, 0, 0, &tbx, &tby, &tbw, &tbh);
  display.setCursor(d.x, (d.y + tbh + 2));
  display.print(d.ctemp);
  if (i == 1) {
    display.setCursor((display.getCursorX() + 5), display.getCursorY());
    display.print(j);
  }
  if (i == 2) {
    display.setCursor(d.x, (display.getCursorY() + getBounds(d.fTemp) + 2));
    display.print(d.fTemp);
  }
}

// Print for the images, text and minmax values (should run once per hour unless
// an error is going on).
void print_ui(void) {
  display.firstPage();
  do {
    display.setFullWindow();

    // Now Y becomes Horizontal, X becomes Vertical.
    display.setRotation(0);
    display.fillScreen(GxEPD_WHITE);

    // Drawing designs on display.
    delay(10);
    display.drawBitmap(drawingTemp.x, drawingTemp.y, drawing_2_black,
                       drawingTemp.w, drawingTemp.h, GxEPD_BLACK);
    delay(10);
    display.drawBitmap(drawingTemp.x, drawingTemp.y, drawing_2_red,
                       drawingTemp.w, drawingTemp.h, GxEPD_RED);
    delay(10);
    display.drawBitmap(drawingHumid.x, drawingHumid.y, drawing_1_black,
                       drawingHumid.w, drawingHumid.h, GxEPD_BLACK);
    delay(10);
    display.drawBitmap(drawingHumid.x, drawingHumid.y, drawing_1_red,
                       drawingHumid.w, drawingHumid.h, GxEPD_RED);

    // Now Y becomes Vertical, X becomes Horizontal.
    display.setRotation(1);
    display.setFont(&FreeMonoBold9pt7b);
    display.setTextColor(GxEPD_BLACK);

    /*
        printTemp(maxTempOutData.ctemp, outSide.maxTemp, temperatureSymbol);
        printTemp(minTempOutData.ctemp, outSide.minTemp, temperatureSymbol);
        printTemp(maxTempInData.ctemp, inSide.maxTemp, temperatureSymbol);
        printTemp(minTempInData.ctemp, inSide.minTemp, temperatureSymbol);
        printTemp(insideTemp.ctemp, inSide.maxTemp, temperatureSymbol);
        printTemp(eindHovenData.ctemp, eindHoven.temperature,
       temperatureSymbol); printTemp(eindHovenData.fTemp, eindHoven.feelTemp,
       temperatureSymbol); sprintf(insideHumid.ctemp, "%d%s", inSide.humidity,
       "%"); printTemp(eLLData.ctemp, eLL.temperature, temperatureSymbol);
        printTemp(eLLData.fTemp, eLL.feelTemp, temperatureSymbol);

        for (int i = 0; i < sizeof(testing); i++)
        {
          rendering(testing[i]);
        }

        rendering(Eindhoven);
        rendering(Ell);
        rendering(Outside_Min);
        rendering(Outside_Max);
        rendering(Inside_Min);
        rendering(Inside_Max);

        //Preparing and drawing all min/max temps on display.
        //Passing 1 to the partialprint function to ensure refresh of the minmax
       values. rendering(maxTempOutData, outSide.maxTime, 1);
        rendering(minTempOutData, outSide.minTime, 1);
        rendering(maxTempInData, inSide.maxTime, 1);
        rendering(minTempInData, inSide.minTime, 1);

        //Current inside temperature.
        rendering(insideTemp);

        //Current inside humidity.
        rendering(insideHumid);

        //Eindhoven temps.
        rendering(eindHovenData, eindHovenData.fTemp, 2);

        //Ell temps.
        rendering(eLLData, eLLData.fTemp, 2);
    */

    // Drawing all text on display.
    if (!secondProbe) {
      rendering(Eindhoven);
      rendering(Ell);
    } else {
      rendering(secondProbeText);
    }

    rendering(Outside_Min);
    rendering(Outside_Max);
    rendering(Inside_Min);
    rendering(Inside_Max);

    // Preparing and drawing all min/max temps on display.
    // Passing 1 to the partialprint function to ensure refresh of the minmax
    // values.
    printTemp(maxTempOutData.ctemp, outSide.maxTemp, temperatureSymbol);
    printTemp(minTempOutData.ctemp, outSide.minTemp, temperatureSymbol);
    printTemp(maxTempInData.ctemp, inSide.maxTemp, temperatureSymbol);
    printTemp(minTempInData.ctemp, inSide.minTemp, temperatureSymbol);
    rendering(maxTempOutData, outSide.maxTime, 1);
    rendering(minTempOutData, outSide.minTime, 1);
    rendering(maxTempInData, inSide.maxTime, 1);
    rendering(minTempInData, inSide.minTime, 1);

    // Current inside temperature.
    printTemp(insideTemp.ctemp, inSide.Temp, temperatureSymbol);
    rendering(insideTemp);

    // Current inside humidity.
    sprintf(insideHumid.ctemp, "%d%s", inSide.humidity, "%");
    rendering(insideHumid);
    if (!secondProbe) {
      // Eindhoven temps.
      printTemp(eindHovenData.ctemp, weatherStationOne.temperature,
                temperatureSymbol);
      printTemp(eindHovenData.fTemp, weatherStationOne.feelTemp,
                temperatureSymbol);
      rendering(eindHovenData, eindHovenData.fTemp, 2);

      // Ell temps.
      printTemp(eLLData.ctemp, weatherStationTwo.temperature,
                temperatureSymbol);
      printTemp(eLLData.fTemp, weatherStationTwo.feelTemp, temperatureSymbol);
      rendering(eLLData, eLLData.fTemp, 2);
    }
    // else
    // {
    //   printTemp(secondProbeData.ctemp, portable.temperature,
    //   temperatureSymbol); rendering(secondProbeData, 2);
    // }

  } while (display.nextPage());
  updateRefresh.outMaxUpdate = updateRefresh.outMinUpdate =
      updateRefresh.inMaxUpdate = updateRefresh.inMinUpdate = 0;

  Serial.println("off");
  socketPrintf("off");
  display.powerOff();
  // delay(2000);
}

// Print for the images, text and minmax values (should run once per hour unless
// an error is going on). Working on making a for loop to print data instead of
// long list of calling the same function.
void print_ui_testing(void) {
  do {
    display.setFullWindow();

    // Now Y becomes Horizontal, X becomes Vertical.
    display.setRotation(0);
    display.fillScreen(GxEPD_WHITE);

    delay(1);
    // Drawing designs on display.
    display.drawBitmap(drawingTemp.x, drawingTemp.y, drawing_2_black,
                       drawingTemp.w, drawingTemp.h, GxEPD_BLACK);
    delay(1);
    display.drawBitmap(drawingTemp.x, drawingTemp.y, drawing_2_red,
                       drawingTemp.w, drawingTemp.h, GxEPD_RED);
    delay(1);
    display.drawBitmap(drawingHumid.x, drawingHumid.y, drawing_1_black,
                       drawingHumid.w, drawingHumid.h, GxEPD_BLACK);
    delay(1);
    display.drawBitmap(drawingHumid.x, drawingHumid.y, drawing_1_red,
                       drawingHumid.w, drawingHumid.h, GxEPD_RED);

    delay(1);
    // Now Y becomes Vertical, X becomes Horizontal.
    display.setRotation(1);
    display.setFont(&FreeMonoBold9pt7b);
    display.setTextColor(GxEPD_BLACK);

    /*
        printTemp(maxTempOutData.ctemp, outSide.maxTemp, temperatureSymbol);
        printTemp(minTempOutData.ctemp, outSide.minTemp, temperatureSymbol);
        printTemp(maxTempInData.ctemp, inSide.maxTemp, temperatureSymbol);
        printTemp(minTempInData.ctemp, inSide.minTemp, temperatureSymbol);
        printTemp(insideTemp.ctemp, inSide.maxTemp, temperatureSymbol);
        printTemp(eindHovenData.ctemp, eindHoven.temperature,
       temperatureSymbol); printTemp(eindHovenData.fTemp, eindHoven.feelTemp,
       temperatureSymbol); sprintf(insideHumid.ctemp, "%d%s", inSide.humidity,
       "%"); printTemp(eLLData.ctemp, eLL.temperature, temperatureSymbol);
        printTemp(eLLData.fTemp, eLL.feelTemp, temperatureSymbol);

        for (int i = 0; i < sizeof(testing); i++)
        {
          rendering(testing[i]);
        }

        rendering(Eindhoven);
        rendering(Ell);
        rendering(Outside_Min);
        rendering(Outside_Max);
        rendering(Inside_Min);
        rendering(Inside_Max);

        //Preparing and drawing all min/max temps on display.
        //Passing 1 to the partialprint function to ensure refresh of the minmax
       values. rendering(maxTempOutData, outSide.maxTime, 1);
        rendering(minTempOutData, outSide.minTime, 1);
        rendering(maxTempInData, inSide.maxTime, 1);
        rendering(minTempInData, inSide.minTime, 1);

        //Current inside temperature.
        rendering(insideTemp);

        //Current inside humidity.
        rendering(insideHumid);

        //Eindhoven temps.
        rendering(eindHovenData, eindHovenData.fTemp, 2);

        //Ell temps.
        rendering(eLLData, eLLData.fTemp, 2);
    */

  } while (display.nextPage());
  updateRefresh.outMaxUpdate = updateRefresh.outMinUpdate =
      updateRefresh.inMaxUpdate = updateRefresh.inMinUpdate = 0;

  Serial.println("off");
  socketPrintf("off");
  display.powerOff();
  // delay(2000);
}
// Function to start a print to the display, with added error check.
// refresh - 1 or 2, 1 is for a complete screen refresh, 2 is for partial screen
// refresh.
void error_check(int refresh) {
  printTemp(testing[14].ctemp, weatherStationTwo.temperature,
            temperatureSymbol);
  Serial.printf("Testing struct array: %s\n", testing[14].ctemp);
  socketPrintf("Testing struct array: %s", testing[14].ctemp);
  // Serial.print("Testing struct array: ");
  // Serial.println(testing[14].ctemp); //elldata
  if (error == 5 && inSide.error == 0 && outSide.error == 0) {
    print_ui();
    error = 0;
    return;
  }
  error = (inSide.error + outSide.error);
  Serial.print("Error Code: ");
  Serial.println(error);
  socketPrintf("Error Code: %d", error);
  switch (refresh) {
  case 1:
    display.clearScreen();
    display.firstPage();
    switch (error) {
    case 0:
      print_ui();
      break;
    case 1:
      print_ui();
      print_inside_data(1);
      break;
    case 2:
      print_ui();
      print_outside_data(1);
      break;
    case 3:
      print_inside_data(1);
      print_outside_data(1);
      break;
    default:
      break;
    }
    break;
  case 2:
    switch (error) {
    case 0:
      print_inside_data(0);
      print_outside_data(0);
      break;
    case 1:
      display.clearScreen();
      display.firstPage();
      print_ui();
      print_inside_data(1);
      break;
    case 2:
      display.clearScreen();
      display.firstPage();
      print_ui();
      print_outside_data(1);
      break;
    case 3:
      display.clearScreen();
      display.firstPage();
      print_inside_data(1);
      print_outside_data(1);
      break;
    default:
      break;
    }
    break;
  }
}

void memoryUsage(void) {
  Serial.println("In memory usage");
  ESP_LOGI(TAG, "RAM left %d", esp_get_free_heap_size());
  if (clientID != 0) {
    char memoryUse[10];
    itoa(esp_get_free_heap_size(), memoryUse, 10);
    socketPrintf("Memory Usage: %s", memoryUse);
  }
}

// void print(void)
// {
//     if (checkPrintTimeH()) // Runs once per hour currently to update the
//     whole display.
//     {
//         updateSensor();
//         if (WifiConnection)
//         {
//             website();
//         }
//         minMaxTemp(WifiConnection);
//         error_check(1);
//         // update();
//         setPreviousMillisHour();
//     }
//     else if (checkPrintTime()) // Runs once per 10 mins currently to only
//     update the values displayed.
//     {
//         if (inSide.error == 1 || outSide.error == 2)
//         {
//             updateSensor();
//             if (WifiConnection)
//             {
//                 website();
//             }
//             minMaxTemp(WifiConnection);
//             error_check(1);
//         }
//         else
//         {
//             updateSensor();
//             if (WifiConnection)
//             {
//                 website();
//             }
//             minMaxTemp(WifiConnection);
//             error_check(2);
//         }
//         memoryUsage();
//         setPreviousMillis();
//     }
//}

void changeDisplay(void) {
  secondProbe = true;
  error_check(1);
}

// For printing info on the display on initial bootup.
void initDisplay(void) {
  display.init();
  // display.hibernate();
  error_check(1);
}
