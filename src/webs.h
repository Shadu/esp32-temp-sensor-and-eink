#ifndef WEBS_H_
#define WEBS_H_

#include <AsyncWebSocket.h>

extern uint32_t clientID;
extern AsyncWebSocket webS;
void socketPrintf(const char *format, ...);
void clientPrintf(uint32_t id, const char *format, ...);


#endif