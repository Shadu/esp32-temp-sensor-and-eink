#ifndef PRINTING_H_
#define PRINTING_H_

void partialPrint();
void initDisplay();
void error_check(int refresh);
void memoryUsage();

#endif