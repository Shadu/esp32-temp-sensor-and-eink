#include "dataUpdate.h"
#include "display.h"
#include "printing.h"
#include "webConnectionHandling.h"
#include "webs.h"
#include "wifi.h"
#include <Arduino.h>

// #define updatePin 26
// #define viewPin 27

TaskHandle_t Task1; // Needed to add a loop to a core of the ESP32.
TaskHandle_t Task2; // Needed to add a loop to a core of the ESP32.
TaskHandle_t Task3; // Needed to add a loop to a core of the ESP32.

// Loop for making sure wifi is connected or starting access point if no
// connection can be made.
void WiFiLoop(void *) {
  for (;;) {
    Serial.println("WifiLoop");
    dnsServer.processNextRequest();
    wifi();
    delay(500);
  }
}

// Loop for displaying data on the e-ink display.
void displayUpdateLoop(void *) {
  for (;;) {
    // updateSensor();
    Serial.println("DisplayUpdateLoop");
    displayUpdate();
    // sleep(5);
    delay(500);
  }
}

// Loop for updating both sensor as station data.
void dataUpdateLoop(void *) {
  for (;;) {
    Serial.println("DataUpdateLoop");
    updateSensor();
    delay(500);
  }
}

// Initial boot.
void setup() {
  Serial.begin(112500);
  /*********Enable Wifi****************/
  wifi();
  serVer();
  dataInit(WifiConnection);
  // pinMode(updatePin, INPUT);
  // pinMode(viewPin, INPUT)
  // //attachInterrupt(digitalPinToInterrupt(viewPin), viewUpdate, CHANGE);
  xTaskCreatePinnedToCore(
      WiFiLoop, "WiFiLoop", 10000, NULL, 1, &Task1,
      1); // Creates a second loop for the 2nd core of the ESP32, allows the
          // device to reconnect to wifi and not interrupt the inside
          // temperature registration.
  xTaskCreatePinnedToCore(
      dataUpdateLoop, "Data Update Loop", 10000, NULL, 1, &Task2,
      1); // Creates a second loop for the 2nd core of the ESP32, allows the
          // device to reconnect to wifi and not interrupt the inside
          // temperature registration.
  initDisplay();
  xTaskCreatePinnedToCore(displayUpdateLoop, "Display Update Loop", 10000, NULL,
                          2, &Task3, 1);
}

// This loop isn't being used.
void loop() {}
