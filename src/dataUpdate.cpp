#include <ArduinoJson.h>
#include <HTTPClient.h>
#include <NTPClient.h>
#include <WEMOS_SHT3X.h>
#include <dataUpdate.h>
#include <list>
#include <webs.h>

WiFiUDP ntpUDP; // initiallizing ntp ot be used to get current time.
NTPClient timeClient(
    ntpUDP, "nl.pool.ntp.org",
    7200); // Time server to get current time from, 3600 for CET, 7200 for CEST.

SHT3X sht30(0x45); // Address to connect to the hardware sensor.

Sens inSide;  // Struct to store data into from the inside sensor.
Sens outSide; // Struct to store min and max temperatures (through website API)
              // and times (ntp API) gotten from the Ell weather station .
stationReading weatherStationOne; // Struct to store data into from the
                                  // weatherwebsite regarding Eindhoven temps.
stationReading weatherStationTwo; // Struct to store data into from the
                                  // weatherwebsite regarding Ell temps.
stationReading portable;
Sap updateRefresh; // Struct to store wanted updates for display data.
// int k = 0;         // Number count for daily reset.

const char *urll =
    "https://data.buienradar.nl/2.0/feed/json"; // Weather website api url.

char tempTime[5]; // Used as temperatory storage to save the digit from the
                  // timeclient to, because passing the right char from the
                  // Sensor struct instantly causes a crash.
char stationOne[50] = "";
char stationTwo[50] = "";
// char charStationList[1500];

const char *station1 = stationOne;
const char *station2 = stationTwo;

// const char *station1 = "Meetstation Eindhoven";
// const char *station2 = "Meetstation Ell";

//  Makes sure the time is always displayed with 2 digits for formatting.
//  j - the char to store the digit into.
//  i - the digit to be stored into the char.
// void time2Digit(char *j, int i) {
//   if (i < 10) {
//   sprintf(j, "%02d", i);
//   } else if (i >= 10) {
//   itoa(i, j, 10);
//   }
// }

//  Makes sure the time is always displayed with 2 digits for formatting.
//  j - the char to store the digit into.
//  i - the digit to be stored into the char.
void time2Digit(char *j, int i) { sprintf(j, "%02d", i); }

// Gets the current time and saves it into a char, formatted with 00:00.
// d - the Sensor struct to save the time into.
void setMaxTimes(Sensor &d) {
  // char tempTime[5];
  bool statusTimeClient = timeClient.update();
  Serial.print("Status Time client: ");
  Serial.println(statusTimeClient);
  time2Digit(tempTime, timeClient.getHours());
  strcpy(d.maxHours, tempTime);
  time2Digit(tempTime, timeClient.getMinutes());
  strcpy(d.maxMins, tempTime);
  sprintf(d.maxTime, "%s:%s", d.maxHours, d.maxMins);
  Serial.println(d.maxTime);
  socketPrintf(d.maxTime);
}

void setMinTimes(Sensor &d) {
  // char tempTime[5];
  bool statusTimeClient = timeClient.update();
  Serial.print("Status Time client: ");
  Serial.println(statusTimeClient);
  Serial.println(timeClient.getHours());
  time2Digit(tempTime, timeClient.getHours());
  strcpy(d.minHours, tempTime);
  time2Digit(tempTime, timeClient.getMinutes());
  strcpy(d.minMins, tempTime);
  sprintf(d.minTime, "%s:%s", d.minHours, d.minMins);
  Serial.println(d.minTime);
  socketPrintf(d.maxTime);
}

// Updates the indoor sensor values,
// sets the display to update the inside temperature if it has changed,
// sets the error value if there is an error encountered.
void updateSensor(void) {
  int sht30Status = sht30.get();
  if (sht30Status != 0) {
    Serial.println("SHT30 Not Connected");
    Serial.print("SHT30 Connection code: ");
    Serial.println(sht30Status);
    inSide.error = 1;
    return;
  }
  // if (sht30Status == 0) {
  Serial.println("SHT30 Connected");
  inSide.error = 0;
  if (((inSide.Temp - sht30.cTemp) >= 0.1 ||
       (sht30.cTemp - inSide.Temp) >= 0.1) &&
      sht30.cTemp < 100) {
    inSide.Temp = sht30.cTemp;
    updateRefresh.inTempUpdate = 1;
  }
  if (((inSide.humidity - sht30.humidity) >= 1 ||
       (sht30.humidity - inSide.humidity) >= 1) &&
      sht30.humidity < 100) {
    inSide.humidity = sht30.humidity;
    updateRefresh.inHumidUpdate = 1;
  }
  // } else {
  //   Serial.println("SHT30 Not Connected");
  //   Serial.print("SHT30 Connection code: ");
  //   Serial.println(sht30Status);
  //   inSide.error = 1;
  // }
}

void maxTempCheck(Sens &location, int *updateRefresh) {
  if (location.Temp > location.maxTemp) {
    location.maxTemp = location.Temp;
    *updateRefresh = 1;
    setMaxTimes(location);
  }
}

void minTempCheck(Sens &location, int *updateRefresh) {
  if (location.Temp < location.minTemp) {
    location.minTemp = location.Temp;
    *updateRefresh = 1;
    setMinTimes(location);
  }
}

void forceTempUpdate(Sens &location, int *updateRefreshMax,
                     int *updateRefreshMin) {
  Serial.print("MAX Temp update: ");
  Serial.println(location.Temp);
  location.maxTemp = location.Temp;
  Serial.print("MIN Temp update: ");
  Serial.println(location.Temp);
  location.minTemp = location.Temp;
  *updateRefreshMin = 1;
  *updateRefreshMax = 1;
  setMaxTimes(location);
  setMinTimes(location);
}

void setOutsideTemps(void) {
  forceTempUpdate(outSide, &updateRefresh.outMaxUpdate,
                  &updateRefresh.outMinUpdate);
}
// Updates the min and max temperature values + times and resets these at
// midnight.
void minMaxTemp(void) {
  static int k = 0;
  if ((timeClient.getHours() == 0 && k >= 10) ||
      k >= 130) // Reset Min-Max Temps
  {
    k = 0;
    forceTempUpdate(inSide, &updateRefresh.inMaxUpdate,
                    &updateRefresh.inMinUpdate);
    forceTempUpdate(outSide, &updateRefresh.outMaxUpdate,
                    &updateRefresh.outMinUpdate);
    return;
  }
  // } else {
  maxTempCheck(inSide, &updateRefresh.inMaxUpdate);
  minTempCheck(inSide, &updateRefresh.inMinUpdate);
  maxTempCheck(outSide, &updateRefresh.outMaxUpdate);
  minTempCheck(outSide, &updateRefresh.outMinUpdate);
  // }
  k += 1;
}

void stationUpdate(float stationValue, float &stationSave, int &displayUpdate) {
  if (stationValue == stationSave) {
    return;
  }
  stationSave = stationValue;
  displayUpdate = 1;
}

JsonVariant weatherStationList(void) {

  // Normal way of transfering station data, but currently broken.
  HTTPClient http;
  http.setReuse(false);
  Serial.print("Weather Station List running on core: ");
  Serial.println(xPortGetCoreID());
  http.begin(urll);
  int httpResponseCode = http.GET();
  outSide.http_error_code = httpResponseCode;
  if (httpResponseCode > 0) {
    Serial.printf("HTTP Response code: %d  \n", httpResponseCode);
    socketPrintf("HTTP Response code: %d", httpResponseCode);
    StaticJsonDocument<96> filter;

    JsonObject filter_actual_stationmeasurements_0 =
        filter["actual"]["stationmeasurements"].createNestedObject();
    filter_actual_stationmeasurements_0["stationname"] = true;

    DynamicJsonDocument doc(8144);
    DeserializationError error = deserializeJson(
        doc, http.getStream(), DeserializationOption::Filter(filter));

    // if (error) {
    //   Serial.print("deserializeJson() failed: ");
    //   Serial.println(error.c_str());
    //   return ;
    // }

    JsonObject actual = doc["actual"];
    JsonArray actual_stationmeasurements = actual["stationmeasurements"];
    DynamicJsonDocument stationList(8144);
    JsonVariant stationListVariant = stationList.to<JsonVariant>();
    for (JsonVariant v : actual_stationmeasurements) {
      stationListVariant.add(v["stationname"].as<const char *>());
    }
    return stationListVariant;
  }
  ///////////////////////////////////////////////////////////
  // Current hacky way of transfering station data, not needed after http
  // requests work normally again.
  // DynamicJsonDocument station(49152);
  // JsonArray stationList = station.to<JsonArray>();
  // deserializeJson(station, charStationList);
  // return stationList;
  ////////////////////////////////////////////
}

// Function that gets the weatherdata via http get from the API supplied.
void website(void) {
  Serial.print("Task Handle?: ");
  Serial.println(xPortGetCoreID());
  if (strcmp(station1, "") == 0 || strcmp(station2, "") == 0) {
    return;
  }
  HTTPClient http;
  http.setReuse(false);
  http.begin(urll);
  int httpResponseCode = http.GET();
  int station1Num = 0;
  int station2Num = 0;
  outSide.http_error_code = httpResponseCode;
  if (httpResponseCode == 0) {
    Serial.printf("Error code: %d  \n", httpResponseCode);
    socketPrintf("Error code: %d", httpResponseCode);
    outSide.error = 2;
    delay(10);
    return;
  }
  StaticJsonDocument<96> filter;

  JsonObject filter_actual_stationmeasurements_0 =
      filter["actual"]["stationmeasurements"].createNestedObject();
  filter_actual_stationmeasurements_0["stationname"] = true;
  filter_actual_stationmeasurements_0["temperature"] = true;
  filter_actual_stationmeasurements_0["feeltemperature"] = true;

  DynamicJsonDocument doc(6144);
  delay(10);
  DeserializationError error = deserializeJson(
      doc, http.getStream(), DeserializationOption::Filter(filter));
  http.end();
  delay(10);
  if (error) {
    Serial.print("deserializeJson() failed: ");
    Serial.println(error.c_str());
    return;
  }

  JsonArray actual_stationmeasurements = doc["actual"]["stationmeasurements"];
  Serial.printf("HTTP Response code: %d  \n", httpResponseCode);
  socketPrintf("HTTP Response code: %d", httpResponseCode);
  JsonObject actual = doc["actual"];
  JsonArray actual_station_improv = doc["actual"]["stationmeasurements"];
  DynamicJsonDocument testing(3072);
  // JsonArray stationList = testing.to<JsonArray>();
  // // serializeJson(actual_stationmeasurements, Serial);
  // for (JsonVariant key : actual_stationmeasurements) {
  //   if (strcmp(key["stationname"].as<const char *>(), station1) == 0 ||
  //       strcmp(key["stationname"].as<const char *>(), station2) == 0) {
  //     for (JsonPair kl : key.as<JsonObject>()) {

  //       Serial.print(kl.key().c_str());
  //       Serial.println(kl.value().as<String>());
  //     }
  //   }
  // }
  for (JsonVariant v : actual_stationmeasurements) {
    delay(10);
    JsonObject stationName =
        testing.createNestedObject(v["stationname"].as<const char *>());
    stationName["temperature"] = v["temperature"].as<float>();
    stationName["feeltemperature"] = v["feeltemperature"].as<float>();
  }
  stationUpdate(testing[station1]["temperature"].as<float>(),
                weatherStationOne.temperature, updateRefresh.ehvUpdate);
  stationUpdate(testing[station1]["feeltemperature"].as<float>(),
                weatherStationOne.feelTemp, updateRefresh.ehvUpdate);
  stationUpdate(testing[station2]["temperature"].as<float>(),
                weatherStationTwo.temperature, updateRefresh.eLLuPdate);
  stationUpdate(testing[station2]["feeltemperature"].as<float>(),
                weatherStationTwo.feelTemp, updateRefresh.eLLuPdate);
  outSide.Temp = testing[station2]["temperature"].as<float>();
  serializeJson(testing[station1]["temperature"], Serial);
  Serial.println("Website");
  Serial.println(weatherStationTwo.feelTemp);
  socketPrintf("Website");
  socketPrintf("%f", weatherStationTwo.feelTemp);
  outSide.error = 0;
  delay(10);
  return;
}

// Initialization of data on bootup.
void dataInit(boolean wifiCon) {
  updateSensor();
  inSide.maxTemp = inSide.Temp;
  inSide.minTemp = inSide.Temp;
  if (wifiCon) {
    timeClient.begin();
    forceTempUpdate(outSide, &updateRefresh.outMaxUpdate,
                    &updateRefresh.outMinUpdate);
    setMaxTimes(inSide);
    setMinTimes(inSide);
    website();
    minMaxTemp();
  }
}
