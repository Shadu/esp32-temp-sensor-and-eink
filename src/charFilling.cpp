#include <Arduino.h>

void addFloat(float f, char *j)
{
  size_t len = strlen(j);
  j += len;
  dtostrf(f, 4, 1, j);
  len = strlen(j);
  j += len;
  strcpy(j, ";");
}

void addChar(const char *c, char *j)
{
  size_t len = strlen(j);
  j += len;
  strcpy(j, c);
  len = strlen(j);
  j += len;
  strcpy(j, ";");
}
