#include "timers.h"
#include <Arduino.h>

long long previousMillis =
    0; // Used to prevent continous activation of a button while pressed.
long long currentMillis =
    0; // Used to prevent continous activation of a button while pressed.
long long previousMillisHour =
    0; // Used to prevent continous activation of a button while pressed.
long long currentMillisHour =
    0; // Used to prevent continous activation of a button while pressed.
static const long long interval =
    (1000000L * 60 * 10); // Used to set update intervals for the display.
static const long long intervalHour =
    (1000000LL * 60 * 60); // Used to set update intervals for the display.
static const long long APMaxRunTime =
    (1000000L * 60 * 5); // Used to set update intervals for the display.
long currentCount =
    0; // Used to prevent continous activation of a button while pressed.
long previousCount =
    0; // Used to prevent continous activation of a button while pressed.
const long intervalCount =
    500; // Used to set required interval between button presses.
long lastDebounce =
    0; // Used to keep track of when the button was last depressed.
long bounceTimer = 50; // Used to set required interval between button presses.
long long APStart = 0;

void setCurrentCount(void) { currentCount = esp_timer_get_time(); }

void setPreviousCount(void) { previousCount = currentCount; }

void setPreviousMillis(void) { previousMillis = currentMillis; }

void setPreviousMillisHour(void) { previousMillisHour = currentMillisHour; }

void lastDebounceTime(void) { lastDebounce = esp_timer_get_time(); }

void apTimerStart(void) { APStart = esp_timer_get_time(); }

void timers(void) {
  currentMillis = esp_timer_get_time();
  currentMillisHour = esp_timer_get_time();
}

boolean checkInfoUpdateTime(void) {
  if (currentCount - previousCount > intervalCount) {
    return true;
  }
  return false;
}

boolean checkPrintTimeH(void) {
  if (currentMillisHour - previousMillisHour > intervalHour) {
    return true;
  }
  return false;
}

boolean checkPrintTime(void) {
  if (currentMillis - previousMillis > interval) {
    return true;
  }
  return false;
}

boolean debounceTimer(void) {
  if (currentMillis - lastDebounce > bounceTimer) {
    return true;
  }
  return false;
}

boolean APUptime(void) {
  if (currentMillis - APStart > APMaxRunTime) {
    return true;
  }
  return false;
}
