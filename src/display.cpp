#include "display.h"
#include "dataUpdate.h"
#include "printing.h"
// #include "timers.h"
#include "timersClass.h"
#include "webs.h"
#include "wifi.h"
#include <Arduino.h>

#define updatePin 26
#define viewPin 27

static const long long intervalHour = (1000000LL * 60 * 60);
long bounceTimert = 50;
static const long long interval10Min = (1000000L * 60 * 10);
const long intervalCount = 500;

Timer tenMinInterval;
Timer hourlyInterval;
Timer debounce;
Timer infoUpdateTimer;

boolean view_debounce = false; // Used to track if the button has been let go.
bool displayUpdateBool = false;
// Keeping track of memory usage to keep track of a potential memory leak.
// void memoryUsage(void)
// {
//     Serial.println("In memory usage");
//     ESP_LOGI(TAG, "RAM left %d", esp_get_free_heap_size());
//     if (clientID != 0)
//     {
//         char memoryUse[10];
//         itoa(esp_get_free_heap_size(), memoryUse, 10);
//         socketPrintf("Memory Usage: %s", memoryUse);
//     }
// }

// Function to call for a partial display update. Called for by the assigned
// button press.
void updateData(void) {
  Serial.print("Update Data running on core: ");
  Serial.println(xPortGetCoreID());
  if (WifiConnection) {
    website();
    delay(10);
    minMaxTemp();
  }
}

void infoUpdate(void) {
  if (digitalRead(updatePin) == HIGH) {
    // setCurrentCount();
    if (infoUpdateTimer.checkTimer(intervalCount)) {
      Serial.printf("Button pressed");
      socketPrintf("Button pressed");
      // updateSensor();
      updateData();
      error_check(2);
      // setPreviousCount();
    }
  }
}

// void changeDisplay(void)
// {
//     if (digitalRead(updatePin) == HIGH && view_debounce == false &&
//     debounceTimer())
//     {
//         secondProbe = true;
//     }
// }

// Function to call for a full display refresh. Called for by the assigned
// button press.
void viewUpdate(void) {
  for (int i = 0; i < 1; i++) {
    if (digitalRead(viewPin) == HIGH && view_debounce == false &&
        debounce.checkTimer(bounceTimert)) {
      // updateSensor();
      updateData();
      error_check(1);
      view_debounce = true;
      // lastDebounceTime();
    }
    if (digitalRead(viewPin) == LOW && view_debounce == true &&
        debounce.checkTimer(bounceTimert)) {
      view_debounce = false;
      // lastDebounceTime();
    }
  }
}
// Function that causes the display to be updated. Gets called by the loop.
void print(void) {
  // if (checkPrintTimeH()) { // Runs once per hour currently to update the
  // whole display.
  Serial.print("Update Display Bool: ");
  Serial.println(displayUpdateBool);
  if (hourlyInterval.checkTimer(intervalHour)) {
    // updateSensor();
    updateData();
    error_check(1);
    // update();
    // setPreviousMillisHour();
  } else if (tenMinInterval.checkTimer(
                 (interval10Min))) // Runs once per 10 mins currently to only
                                   // update the values displayed.
  {
    if (inSide.error == 1 || outSide.error == 2) {
      // updateSensor();
      updateData();
      error_check(1);
    } else {
      // updateSensor();
      updateData();
      error_check(2);
    }
    memoryUsage();
    // setPreviousMillis();
  }
  if (displayUpdateBool == true) {
    Serial.println("DisplayUpdateBool was set to True!");
    // setTemps();
    error_check(1);
    displayUpdateBool = false;
  }
}

void displayUpdate(void) {
  infoUpdate();
  viewUpdate();
  print();
  // timers();
}
