#include "timersClass.h"
#include <Arduino.h>

long long previousMillisnu = 0;
long long intervalnu = 0;
long long currentMillisnu = 0;

bool Timer::checkTimer(long long passedInterval) {
  currentMillis = esp_timer_get_time();
  boolean result;
  if (currentMillis - previousMillis > passedInterval) {
    previousMillis = currentMillis;
    result = true;
  } else {
    result = false;
  }
  return result;
}
