#ifndef TIMERS_H_
#define TIMERS_H_
#include <stdbool.h>

void setCurrentCount(void);
void setPreviousCount(void);
void setPreviousMillis(void);
void setPreviousMillisHour(void);
void lastDebounceTime(void);
void apTimerStart(void);
void timers(void);
bool checkInfoUpdateTime(void);
bool checkPrintTimeH(void);
bool checkPrintTime(void);
bool debounceTimer(void);
bool APUptime(void);

#endif
