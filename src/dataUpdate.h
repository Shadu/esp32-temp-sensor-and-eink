#ifndef DATAUPDATE_H_
#define DATAUPDATE_H_

#include <ArduinoJson.h>
#include <stdbool.h>

typedef struct Sensor {
  float Temp;
  // char cTemp[6];
  float maxTemp;
  // char cmaxTemp[6];
  float minTemp;
  // char cminTemp[6];
  int humidity;
  // char chumidity[4];
  int error;
  int maxUpdate;
  int minUpdate;
  char maxHours[4];
  char maxTime[7];
  char minTime[7];
  char minHours[4];
  char maxMins[4];
  char minMins[4];
  int http_error_code;
} Sens;

typedef struct dataUpdate {
  int inMaxUpdate;
  int inMinUpdate;
  int outMaxUpdate;
  int outMinUpdate;
  int eLLuPdate;
  int ehvUpdate;
  int inTempUpdate;
  int inHumidUpdate;
  int insideMaxUpdate;
} Sap;

typedef struct stationReading {
  const char *stationName;
  float temperature;
  float groundTemp;
  float feelTemp;
} StationRead;

void updateSensor();
void dataInit(bool wifiConnection);
void website();
void minMaxTemp();
void setOutsideTemps();
JsonVariant weatherStationList();
extern Sens inSide;
extern Sens outSide;
extern StationRead weatherStationOne;
extern StationRead weatherStationTwo;
extern StationRead portable;
extern Sap updateRefresh;
extern char stationOne[50];
extern char stationTwo[50];

#endif
