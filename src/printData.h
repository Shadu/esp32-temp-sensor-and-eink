#ifndef PRINTDATA_H_
#define PRINTDATA_H_

#include <stdint.h>

struct printData
{
    const char *ctemp;
    int x;
    int y;
};

struct minMaxData
{
    uint16_t x;
    uint16_t y;
    uint16_t w;
    uint16_t h;
    char ctemp[13];
    char fTemp[13];
    char time[6];
    int updated;
};

struct errorMessage
{
    uint16_t x;
    uint16_t y;
    uint16_t yY;
    uint16_t w;
    uint16_t h;
    char message1[13];
    char message2[13];
    char message3[13];
};

extern const char * temperatureSymbol;

extern minMaxData testing[18];

extern minMaxData Eindhoven;
extern minMaxData Ell;
extern minMaxData secondProbeText;
extern minMaxData Outside_Min;
extern minMaxData Outside_Max;
extern minMaxData Inside_Min;
extern minMaxData Inside_Max;

extern minMaxData maxTempInData;
extern minMaxData minTempInData;
extern minMaxData maxTempOutData;
extern minMaxData minTempOutData;
extern minMaxData insideTemp;
extern minMaxData insideHumid;
extern minMaxData eindHovenData;
extern minMaxData eLLData;
extern minMaxData secondProbeData;
extern minMaxData drawingTemp;
extern minMaxData drawingHumid;

extern minMaxData errorOut;
extern minMaxData errorIn;

#endif