#ifndef WEBCONNECTIONHANDLING_H_
#define WEBCONNECTIONHANDLING_H_

#include <AsyncWebSocket.h>
#include <DNSServer.h>

extern DNSServer dnsServer;

void onWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client,
               AwsEventType type, void *arg, uint8_t *data, size_t len);
void serVer();

#endif
