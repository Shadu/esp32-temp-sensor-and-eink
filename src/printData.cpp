#include "printData.h"

const char * temperatureSymbol = "C";               //Symbol to be used with temperatures.

/*printData Eindhoven = {"Eindhoven", 0, 0};          //UI tekst to display and location
printData Ell = {"Ell", 110, 0};                    //UI tekst to display and location
printData Outside_Max = {"Outside Max", 170, 0};    //UI tekst to display and location
printData Outside_Min = {"Outside Min", 170, 32};   //UI tekst to display and location
printData Inside_Max = {"Inside Max", 170, 64};     //UI tekst to display and location
printData Inside_Min = {"Inside Min", 170, 96};     //UI tekst to display and location*/
//minMaxData testing[18];
//minMaxData testing[1] = {0, 0, 0, 0, "Eindhoven"};
//minMaxData testing[18];// = {{0, 0, 0, 0, "Eindhoven"}, {110, 0, 0, 0,"Ell"}};                    //UI tekst to display and location

minMaxData testing[18] = {{170, 0, 0, 0,"Outside Max"}, {110, 0, 0, 0,"Ell"}, {170, 32, 0, 0, "Outside Min"}, {170, 64, 0, 0, "Inside Max"}, {170, 96, 0, 0, "Inside Min"}, {170, 16, 126, 16}, {170, 48, 126, 16}, {170, 80, 116, 16}, {170, 112, 116, 16}, {55, 64, 55, 16}, {138, 64, 32, 16}, {0, 16, 55, 32}, {110, 16, 55, 32}, {0, 0, 60, 63}, {0, 110, 60, 41}, {0, 0, 170, 64, "Error with", "outside data"} ,{0, 64, 170, 64, "Error with", "temp sens"}};                    //UI tekst to display and location

minMaxData Eindhoven = {0, 0, 0, 0, "Eindhoven"};
minMaxData Ell = {110, 0, 0, 0,"Ell"};                    //UI tekst to display and location
minMaxData secondProbeText = {0, 0, 0, 0, "SecondProbe"};
minMaxData Outside_Max = {170, 0, 0, 0,"Outside Max"};    //UI tekst to display and location
minMaxData Outside_Min = {170, 32, 0, 0, "Outside Min"};   //UI tekst to display and location
minMaxData Inside_Max = {170, 64, 0, 0, "Inside Max"};     //UI tekst to display and location
minMaxData Inside_Min = {170, 96, 0, 0, "Inside Min"};     //UI tekst to display and location


minMaxData maxTempOutData = {170, 16, 126, 16};     //Struct to that has location to print, what to print and whether to print.
minMaxData minTempOutData = {170, 48, 126, 16};     //Struct to that has location to print, what to print and whether to print.
minMaxData maxTempInData = {170, 80, 116, 16};      //Struct to that has location to print, what to print and whether to print.
minMaxData minTempInData = {170, 112, 116, 16};     //Struct to that has location to print, what to print and whether to print.
minMaxData insideTemp = {55, 64, 55, 16};           //Struct to that has location to print, what to print and whether to print.
minMaxData insideHumid = {138, 64, 32, 16};         //Struct to that has location to print, what to print and whether to print.
minMaxData eindHovenData = {0, 16, 55, 32};         //Struct to that has location to print, what to print and whether to print.
minMaxData eLLData = {110, 16, 55, 32};             //Struct to that has location to print, what to print and whether to print.
minMaxData secondProbeData = {0, 16, 55, 32};
minMaxData drawingTemp = {0, 0, 60, 63};            //Struct to that has location to print, what to print and whether to print.
minMaxData drawingHumid = {0, 110, 60, 41};         //Struct to that has location to print, what to print and whether to print.

minMaxData errorOut = {0, 0, 170, 64, "Error with", "outside data"};    //Struct to that has location to print, what to print.
minMaxData errorIn = {0, 64, 170, 64, "Error with", "temp sens"};       //Struct to that has location to print, what to print.
