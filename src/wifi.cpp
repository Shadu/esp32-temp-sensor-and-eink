#include "wifi.h"
// #include "timers.h"
#include "timersClass.h"
#include <SPI.h>

#include <Preferences.h>
#include <WiFi.h>

static const long long APMaxRunTime =
    (1000000L * 60 * 5); // Used to set update intervals for the display.

String diss;
String wordPass;
Timer WifiTimer;

Preferences preferences;
// boolean WiFiCredentialsExist;
boolean WifiConnection = false;
boolean wifiConFailBool = false;
boolean WifiCredsRead = false;
boolean WifiCredsAdjusted = false;

const char *hostname = "Zolder";
const char *APssid = "TempSens";
// const char *passw;

// const char *password;
// String blok = "16051996";
char passchar[20];
char ssidchar[20];
const char *passw = passchar;
const char *ssid = ssidchar;

const char *test;

int wifiConFail = 0;
int passint = 0;
int APClients = 0;

IPAddress local_ip(192, 168, 1, 110);

void wifiConnectionFail(boolean j) {
  if (j) {
    // Serial.print("Connection failed, times: ");
    // Serial.println(wifiConFail);
    wifiConFail++;
    if (wifiConFail >= 3) {
      // Serial.println("3 Connections failed.");
      wifiConFailBool = true;
      wifiConFail = 0;
    }
  } else if (!j) {
    Serial.println("Connection succes");
    wifiConFail = 0;
    wifiConFailBool = false;
  }
}

boolean checkConnection(void) {
  // Serial.print("Check Connection running on core: ");
  // Serial.println(xPortGetCoreID());
  // Serial.println("Checking Connection: ");
  // Serial.print("Wifi status: ");
  WifiConnection = WiFi.status() == WL_CONNECTED;
  // Serial.println(WiFi.status());
  // if (WiFi.status() == WL_CONNECTED) {
  // WifiConnection = true;
  // Serial.println("Connected");
  // } else {
  // WifiConnection = false;
  // Serial.println("No Connection");
  // }
  return WifiConnection;
}

void WiFiEvent(WiFiEvent_t event) {
  switch (event) {
  case ARDUINO_EVENT_WIFI_AP_STACONNECTED:
    // case SYSTEM_EVENT_AP_STACONNECTED:
    APClients++;
    break;
  }
}

void startAP(void) {
  // Serial.println("Starting AcessPoint.");
  APClients = 0;
  // apTimerStart();
  WiFi.disconnect();
  WiFi.softAP(APssid);
  WiFi.onEvent(WiFiEvent);
  wifiConnectionFail(false);
  WifiConnection = false;
  // checkConnection();
}

boolean CheckWiFiCredentials(void) {
  if (strcmp(ssid, "") == 0 || strcmp(passw, "") == 0) {
    // if (ssid == "" || passw == "")
    //{
    //  Serial.println("One of the credentials failed.");
    //  Serial.print("Saved SSID: ");
    //  Serial.println(ssid);
    //  Serial.print("Saved password: ");
    //  Serial.println(passw);
    if (WiFi.getMode() != 3) {
      startAP();
      return false;
    } else if (WiFi.getMode() == 3) {
      return false;
    }
    // WiFiCredentialsExist = false;
  }
  // Serial.print("Saved SSID: ");
  // Serial.println(ssid);
  // Serial.print("Saved password: ");
  // Serial.println(passw);
  // Serial.print("Char SSID: ");
  // Serial.println(ssid);
  // Serial.println("blaaaaat");
  // //String blop = "16051996";
  // //password = blop.toCharArray(blop, );
  // //Serial.println(password);
  // //blok.toCharArray(passchar, 20);
  // Serial.print("Char passw: ");
  // Serial.println(passw);
  // Serial.print("Char test: ");
  // Serial.println(test);
  // Serial.print("Char passchar: ");

  // Serial.println(passchar);
  return true;
  // WiFiCredentialsExist = true;
  // return WiFiCredentialsExist;
}

void getWiFiCredentials(void) {
  // Serial.println("Getting credentials.");
  preferences.begin("credentials", true);
  // ssid = preferences.getString("ssid", "NoSSID").c_str();
  preferences.getString("ssid", "NoSSID").toCharArray(ssidchar, 20);
  // passw = preferences.getString("password").c_str();
  preferences.getString("password").toCharArray(passchar, 20);
  // passint = preferences.getString("password").toInt();
  // itoa(passint, passchar, 10);
  // diss = "hallo";
  // test = diss.c_str();
  // diss = preferences.getString("ssid", "NoSSID");
  // wordPass = preferences.getString("password", "NoPASS");
  preferences.end();
  WifiCredsRead = true;
}

boolean waitForConnection(void) {
  // Serial.println("Waiting for connection.");
  int i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("WiFi Connecting..");

    if (i >= 10) {
      // startAP();
      wifiConnectionFail(true);
      return false;
      // break;
    }
    i++;
  }
  wifiConnectionFail(false);
  checkConnection();
  return true;
}

void connectToAp(void) {
  WiFi.mode(WIFI_OFF);
  // Serial.println("Connecting to wifi.");
  if (!WifiCredsRead) {
    // Serial.println("Getting credentials");
    getWiFiCredentials();
  }
  if (CheckWiFiCredentials()) {
    // Serial.println("Initializing connection to wifi");
    // WiFi.config(local_ip, INADDR_NONE, INADDR_NONE, INADDR_NONE);
    WiFi.setHostname(hostname);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, passw);
    // WiFi.begin(diss.c_str(), wordPass.c_str());
    waitForConnection();
  }
}

boolean checkAPClients(void) {
  if (WiFi.softAPgetStationNum() != 0) {
    // Serial.println("Clients are connected");
    return true;
  } else {
    if ((APClients > 0 || WifiTimer.checkTimer(APMaxRunTime)) &&
        CheckWiFiCredentials()) {
      // Serial.println("No Clients are connected");
      connectToAp();
      return false;
    }
  }
}

void wifiStatus(void) {
  Serial.println("Wifi connection info.");
  Serial.print("Connected to wifi: ");
  Serial.println(WiFi.isConnected());
  Serial.print("Access Point connected to: ");
  Serial.println(WiFi.SSID());
  Serial.print("IP Address Assigned: ");
  Serial.println(WiFi.localIP());
  Serial.print("SSID Saved: ");
  Serial.println(ssid);
  Serial.print("Password Saved: ");
  Serial.println(passw);
}

void wifi(void) {
  delay(2000);
  // Serial.println("Checking wifi.");
  // Serial.print("Wifi loop core: ");
  // Serial.println(xPortGetCoreID());
  if (WifiCredsAdjusted == true) {
    WifiCredsAdjusted = false;
    connectToAp();
  } else if (!checkConnection() && !wifiConFailBool) {
    switch (WiFi.getMode()) {
    case 0:
      Serial.println("Connecting to AP from switch.");
      connectToAp();
      break;
    case 1:
      Serial.println(
          "Connecting to AP from switch while previously being connected.");
      connectToAp();
      break;
    case 3:
      Serial.println("Starting and checking self access point mode.");
      checkAPClients();
      break;
    default:
      break;
    }
  } else if (WiFi.getMode() != 3 && !checkConnection() && wifiConFailBool) {
    Serial.println("Starting AccessPoint Mode.");
    startAP();
  }

  // Serial.print("Wifi Mode: ");
  // Serial.println(WiFi.getMode());
}

void adjustWifiConnection(const char *s, const char *k) {
  Serial.print("Updating Credentials: ");
  Serial.print(s);
  Serial.println(k);
  Serial.println("Updated Creds.");
  preferences.begin("credentials", false);
  preferences.putString("ssid", s);
  preferences.putString("password", k);
  Serial.println(preferences.getString("ssid", ""));
  Serial.println(preferences.getString("password", ""));
  preferences.end();
  WifiCredsRead = false;
  WifiCredsAdjusted = true;
  // connectToAp();
}
