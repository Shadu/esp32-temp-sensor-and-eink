#define DYNAMIC_JSON_DOCUMENT_SIZE 2048

#include <ArduinoJson.h>
#include <AsyncJson.h>
#include <AsyncWebSocket.h>
#include <DNSServer.h>
#include <ESPAsyncWebServer.h>

#include "charFilling.h"
#include "dataUpdate.h"
#include "display.h"
#include "printing.h"
#include "webConnectionHandling.h"
#include "webs.h"
#include "website.h"
#include "wifi.h"

DNSServer dnsServer;
AsyncWebServer server(80);
AsyncEventSource events("/events");
TaskHandle_t displayUpdateTask;

void memoryUsageTransmit(AsyncWebSocketClient *client) {
  char memory[20];
  char *ptr = memory;
  strcpy(ptr, "memoryUsage;");
  size_t len = strlen(ptr);
  ptr += len;
  itoa(esp_get_free_heap_size(), ptr, 10);
  client->printf(memory);
}

void sensorDataTransmit(AsyncWebSocketClient *client) {
  char insideInfo[24];
  char *ptr = insideInfo;
  dtostrf(inSide.Temp, 4, 1, ptr);
  size_t len = strlen(ptr);
  ptr += len;
  strcpy(ptr, ";");
  len = strlen(ptr);
  ptr += len;
  itoa(inSide.humidity, ptr, 10);
  client->printf("%s", insideInfo); // Sends Data as "Temperature;Humidity"
}

void httpCodeTransmit(AsyncWebSocketClient *client) {
  Serial.printf("Error code: %d  \n", outSide.http_error_code);
  socketPrintf("Error Code: %d", outSide.http_error_code);
  char errorCode[10];
  char *ptr = errorCode;
  strcpy(ptr, "error;");
  size_t len = strlen(ptr);
  ptr += len;
  itoa(outSide.http_error_code, ptr, 10);
  client->printf(errorCode);
}

void outsideDataTransmit(AsyncWebSocketClient *client) {
  char minMaxInfo[50];
  char *ptr = minMaxInfo;
  dtostrf(inSide.maxTemp, 4, 1, ptr);
  size_t len = strlen(ptr);
  ptr += len;
  strcpy(ptr, ";");
  addChar(inSide.maxTime, ptr);
  addFloat(inSide.minTemp, ptr);
  addChar(inSide.minTime, ptr);
  addFloat(outSide.maxTemp, ptr);
  addChar(outSide.maxTime, ptr);
  addFloat(outSide.minTemp, ptr);
  addChar(outSide.minTime, ptr);
  Serial.println(minMaxInfo);
  client->printf(minMaxInfo);
}

void wifiConnectionUpdate(uint8_t *data) {
  char *msgssid;
  char *msgpassw;
  msgssid = strtok(NULL, "|");
  msgpassw = strtok(NULL, "|");
  Serial.print("Printing F: ");
  Serial.println(msgssid);
  Serial.println(msgpassw);
  const char *sid = msgssid;
  const char *passw = msgpassw;
  adjustWifiConnection(sid, passw);
  WiFi.softAPdisconnect(true);
}

void setDebugClient(AsyncWebSocketClient *client) {
  Serial.println("succes 15");
  clientID = client->id();
  Serial.printf("Client ID setting: %u  \n", clientID);
}

void webSocketMessageCodeHandling(int j, AsyncWebSocketClient *client,
                                  uint8_t *data) {
  switch (j) {
  case 10:
    sensorDataTransmit(client);
    break;
  case 11:
    httpCodeTransmit(client);
    break;
  case 12:
    memoryUsageTransmit(client);
    break;
  case 13:
    outsideDataTransmit(client);
    break;
  case 14:
    break;
  case 15:
    setDebugClient(client);
    break;
  case 20:
    wifiConnectionUpdate(data);
    break;
  }
}

void onWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client,
               AwsEventType type, void *arg, uint8_t *data, size_t len) {
  Serial.print("Websocket Handler Core: ");
  Serial.println(xPortGetCoreID());
  if (type == WS_EVT_CONNECT) {
    // client connected
    Serial.printf("ws[%s][%u] connect  \n", server->url(), client->id());
  } else if (type == WS_EVT_DISCONNECT) {
    // client disconnected
    if (client->id() == clientID) {
      clientID = 0;
    }
    Serial.printf("ws[%s][%u] disconnect: %u  \n", server->url(), client->id(),
                  client->id());
  } else if (type == WS_EVT_ERROR) {
    // error was received from the other end
    Serial.printf("ws[%s][%u] error(%u): %s  \n", server->url(), client->id(),
                  *((uint16_t *)arg), (char *)data);
  } else if (type == WS_EVT_PONG) {
    // pong message was received (in response to a ping request maybe)
    Serial.printf("ws[%s][%u] pong[%u]: %s  \n", server->url(), client->id(),
                  len, (len) ? (char *)data : "");
  } else if (type == WS_EVT_DATA) {
    // data packet
    AwsFrameInfo *info = (AwsFrameInfo *)arg;
    if (info->final && info->index == 0 && info->len == len) {
      // how to send multiple frame data?/////////////////////////////////////

      // the whole message is in a single frame and we got all of it's data
      Serial.printf("ws[%s][%u] %s-message[%llu]:  \n", server->url(),
                    client->id(), (info->opcode == WS_TEXT) ? "text" : "binary",
                    info->len);
      if (info->opcode == WS_TEXT) {
        data[len] = 0;
        Serial.printf("%s  \n", (char *)data);

        // Getting the Message Identifier from the data. (First 2 numbers,
        // seperated from the rest of the message by a | ).
        char *msgidentifier;
        msgidentifier = strtok((char *)data, "|");
        int intmsgidentifier = strtol(msgidentifier, NULL, 0);

        Serial.print("Printing the int msgidentifier");
        Serial.println(intmsgidentifier);
        socketPrintf("It does this.");
        Serial.println("It does this.");
        Serial.println((char *)data);

        // Switch to go through based on the Message Identifier.
        webSocketMessageCodeHandling(intmsgidentifier, client, data);

        Serial.printf("Websocket data received: %s  \n", data);
        socketPrintf("Websocket data received: %s", data);
        Serial.printf("Client ID: %u  \n", client->id());
        socketPrintf("Client ID: %u", client->id());
      } else {
        for (size_t i = 0; i < info->len; i++) {
          Serial.printf("%02x ", data[i]);
          socketPrintf("%02x ", data[i]);
        }
        Serial.printf("  \n");
      }
    } else {
      // message is comprised of multiple frames or the frame is split into
      // multiple packets
      if (info->index == 0) {
        if (info->num == 0)
          Serial.printf("ws[%s][%u] %s-message start\n", server->url(),
                        client->id(),
                        (info->message_opcode == WS_TEXT) ? "text" : "binary");
        Serial.printf("ws[%s][%u] frame[%u] start[%llu]\n", server->url(),
                      client->id(), info->num, info->len);
      }

      Serial.printf("ws[%s][%u] frame[%u] %s[%llu - %llu]: ", server->url(),
                    client->id(), info->num,
                    (info->message_opcode == WS_TEXT) ? "text" : "binary",
                    info->index, info->index + len);
      if (info->message_opcode == WS_TEXT) {
        data[len] = 0;
        Serial.printf("%s\n", (char *)data);
      } else {
        for (size_t i = 0; i < len; i++) {
          Serial.printf("%02x ", data[i]);
        }
        Serial.printf("\n");
      }

      if ((info->index + len) == info->len) {
        Serial.printf("ws[%s][%u] frame[%u] end[%llu]\n", server->url(),
                      client->id(), info->num, info->len);
        if (info->final) {
          Serial.printf("ws[%s][%u] %s-message end\n", server->url(),
                        client->id(),
                        (info->message_opcode == WS_TEXT) ? "text" : "binary");
          if (info->message_opcode == WS_TEXT)
            client->text("I got your text message2");
          else
            client->binary("I got your multi frame binary message");
        }
      }
    }
  }
}

void wifiConnectionUpdate(DynamicJsonDocument doc) {
  const char *sid = doc["ssid"];
  const char *passw = doc["passw"];
  Serial.println(sid);
  Serial.println(passw);
  // adjustWifiConnection(sid, passw);
  // WiFi.softAPdisconnect(true);
}

void displayTaskStart(void *) { error_check(1); }

void stationUpdate(DynamicJsonDocument doc) {
  Serial.print("Station Update on core: ");
  Serial.println(xPortGetCoreID());
  strncpy(stationOne, doc["stationOne"], 50);
  strncpy(stationTwo, doc["stationTwo"], 50);
  updateData();
  setOutsideTemps();
  displayUpdateBool = true;

  Serial.println(stationOne);
}

void stationListShow(AsyncWebServerRequest *request) {
  DynamicJsonDocument station(8144);
  // JsonArray stationList = station.to<JsonArray>();
  JsonVariant stationListVariant = station.to<JsonVariant>();
  stationListVariant.set(weatherStationList());
  // Needed for the hacky way of transfering the station data, not needed after
  // http requests work normally again.
  // char testing[1500];
  // serializeJson(receivedStationList, testing);
  // deserializeJson(station, testing);
  /////////////////////////////////////////////////////

  // Normal way of transfering the station data without being a hacky way, but
  // current broken.
  // int arraySize = receivedStationList.size();
  // for (int i = 0; i < arraySize; i++) {
  //   delay(10);
  //   Serial.print("Memory Usage during stationlist: ");
  //   Serial.println(esp_get_free_heap_size());
  //   stationList.add(receivedStationList[i]);
  // }
  ////////////////////////////////////////////////////
  AsyncJsonResponse *jsonResponse = new AsyncJsonResponse();
  jsonResponse->addHeader("Server", "ESP Async Web Server");
  JsonVariant &root = jsonResponse->getRoot();
  root.set(stationListVariant);
  jsonResponse->setLength();
  request->send(jsonResponse);
  // AsyncResponseStream *response =
  //     request->beginResponseStream("application/json");
  // // serializeJsonPretty(station, *response);
  // serializeJsonPretty(receivedStationList, *response);
  // Serial.println("Sending Response!");
  // request->send(response);
}

void httpMessageHandling(int messageID, DynamicJsonDocument doc,
                         AsyncWebServerRequest *request) {
  Serial.print("Switch: ");
  Serial.println(doc["ssid"].as<const char *>());
  switch (messageID) {
  case 1:
    request->send(200);
    wifiConnectionUpdate(doc);
    break;
  case 2:
    request->send(200);
    stationUpdate(doc);
    break;
  case 3:
    stationListShow(request);
    break;
  }
}

void serVer() {
  Serial.print("Server running on core: ");
  Serial.println(xPortGetCoreID());
  dnsServer.start(53, "*", WiFi.softAPIP());
  webS.onEvent(onWsEvent);
  server.addHandler(&webS);
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/html", html_1);
  });
  server.on(
      "/post", HTTP_POST, [](AsyncWebServerRequest *request) {}, NULL,
      [](AsyncWebServerRequest *request, uint8_t *data, size_t len,
         size_t index, size_t total) {
        DynamicJsonDocument doc(1024);
        deserializeJson(doc, (char *)data);
        Serial.print("Data: ");
        int messageID = (int)doc["identifier"];
        Serial.print("Server: ");
        Serial.println(doc["ssid"].as<const char *>());

        httpMessageHandling(messageID, doc, request);
      });
  server.begin();
  // attachInterrupt(digitalPinToInterrupt(viewPin), viewUpdate, CHANGE);
  Serial.println("Server Created");
}
