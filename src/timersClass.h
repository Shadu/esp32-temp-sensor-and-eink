#ifndef TIMERSCLASS_H_
#define TIMERSCLASS_H_

class Timer {

private:
  long long previousMillis;
  long long interval;
  long long currentMillis;

public:
  bool checkTimer(long long passedInterval);
};
#endif
